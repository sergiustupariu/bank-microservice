package com.microservices.account.controller;

import com.microservices.account.exception.OutsideOpeningHoursException;
import com.microservices.account.model.SavingAccount;
import com.microservices.account.service.SavingAccountServiceInterface;
import com.microservices.account.utils.DateTimeUtils;
import java.util.List;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/account")
public class SavingAccountController {

	//TODO future improvement: inject the cached version of the service for performance optimization
	@Autowired
	@Qualifier("savingAccountServiceRaw")
	SavingAccountServiceInterface savingAccountService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> create(@RequestBody SavingAccount savingAccount){
		LocalDateTime localDateTime = LocalDateTime.now();
		if (!DateTimeUtils.isEntryTimeValid(localDateTime)) {
			throw new OutsideOpeningHoursException("Operation cannot be performed outside working hours and on weekends!");
		}

		this.savingAccountService.create(savingAccount);

		return new ResponseEntity(HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/{accountId}")
	public SavingAccount get(@PathVariable String accountId){
		return this.savingAccountService.get(accountId);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="")
	public List<SavingAccount> getAll(){
		return this.savingAccountService.getAll();
	}
	
}
