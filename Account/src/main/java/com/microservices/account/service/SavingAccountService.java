package com.microservices.account.service;

import java.util.List;
import java.time.LocalDateTime;
import java.time.DayOfWeek;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.microservices.account.model.SavingAccount;
import com.microservices.account.dao.SavingAccountRepository;

@Component("savingAccountServiceRaw")
public class SavingAccountService implements SavingAccountServiceInterface {

	@Autowired
	SavingAccountRepository savingAccountRepository;

	public SavingAccount create(SavingAccount savingAccount){
		SavingAccount result = savingAccountRepository.save(savingAccount);
		return result;
	}

	public SavingAccount get(String accountId){
		return savingAccountRepository.findOne(accountId);
	}

	public List<SavingAccount> getAll(){
		return savingAccountRepository.findAll();
	}
	
}
