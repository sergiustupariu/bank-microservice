package com.microservices.account.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.microservices.account.model.SavingAccount;

/**
 * TODO possible future feature: inject MemCache or Redis service here,
 * compute the cache keys and use caching.
 * 
 */
@Component("savingAccountServiceCacheDecorator")
public class SavingAccountServiceCacheDecorator implements SavingAccountServiceInterface {

	@Autowired
	SavingAccountService savingAccountService;

	public SavingAccount create(SavingAccount savingAccount){
		//TODO invalidate cache for the global (getAll()) result
		SavingAccount result = savingAccountService.create(savingAccount);
		return result;
	}

	public SavingAccount get(String accountId){
		// TODO if resource for this accountId is found in cache => use cache, otherwise call raw service
		return savingAccountService.get(accountId);
	}

	public List<SavingAccount> getAll(){
		// TODO: return cached version
		return savingAccountService.getAll();
	}
	
}
