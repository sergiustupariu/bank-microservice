package com.microservices.account.service;

import java.util.List;

import org.springframework.stereotype.Component;
import com.microservices.account.model.SavingAccount;

@Component
public interface SavingAccountServiceInterface {

	public SavingAccount create(SavingAccount savingAccount);

	public SavingAccount get(String accountId);

	public List<SavingAccount> getAll();	
}
