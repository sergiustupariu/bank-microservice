package com.microservices.account.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.microservices.account.model.SavingAccount;

public interface SavingAccountRepository extends MongoRepository<SavingAccount, String> {

}
