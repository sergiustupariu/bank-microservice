package com.microservices.account.utils;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.DayOfWeek;

public class DateTimeUtils {

	public static final LocalTime OPENING_TIME = LocalTime.of(9, 0);
	public static final LocalTime CLOSING_TIME = LocalTime.of(17, 0);

	public static boolean isEntryTimeValid(LocalDateTime entryTime) {
		DayOfWeek dayOfWeek = entryTime.getDayOfWeek();
		LocalTime localTime = entryTime.toLocalTime();
		boolean entryTimeValid = true;

		if (DayOfWeek.SATURDAY.equals(dayOfWeek) || DayOfWeek.SUNDAY.equals(dayOfWeek)) {
			entryTimeValid = false;
		} else if (localTime.isBefore(OPENING_TIME) || localTime.isAfter(CLOSING_TIME)) {
			entryTimeValid = false;
		}

		return entryTimeValid;
	}
}
