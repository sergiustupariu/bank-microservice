package com.microservices.account.exception;

import org.springframework.http.HttpStatus;
import java.lang.Throwable;

public class ApiError {

   private HttpStatus status;
   private String message;

   public ApiError(HttpStatus status, String message) {
	   this.status = status;
       this.message = message;
   }

   public ApiError(HttpStatus status, Throwable ex) {
	   this.status = status;
       this.message = "Unexpected error";
   }
   
   public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

   public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}