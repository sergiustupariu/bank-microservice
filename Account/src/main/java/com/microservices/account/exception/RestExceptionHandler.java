package com.microservices.account.exception;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

   @ExceptionHandler(DuplicateKeyException.class)
   protected ResponseEntity<Object> handleDuplicateKeyException(DuplicateKeyException ex, WebRequest request) {
	   String message = "Each user can have only one savings account.";
	   return handleBadRequestException(message);
   }

   @ExceptionHandler(OutsideOpeningHoursException.class)
   protected ResponseEntity<Object> handleOutsideOpeningHoursException(OutsideOpeningHoursException ex, WebRequest request) {
	   return handleBadRequestException(ex.getMessage());
   }

   @Override
   protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
																           HttpHeaders headers,
																           HttpStatus status,
																           WebRequest request) {

       return handleBadRequestException("Bad input");
   }
   
   private ResponseEntity<Object> handleBadRequestException(String message) {
	   ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, message);
       return buildResponseEntity(apiError);
   }

   private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
       return new ResponseEntity<>(apiError, apiError.getStatus());
   }
}