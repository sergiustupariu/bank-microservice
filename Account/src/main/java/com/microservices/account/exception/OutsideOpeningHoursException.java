package com.microservices.account.exception;

import org.springframework.http.HttpStatus;
import java.lang.RuntimeException;

public class OutsideOpeningHoursException extends RuntimeException {

   public OutsideOpeningHoursException(String message) {
	   super(message);
   }
}