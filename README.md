
Tools and Frameworks used:

- Spring Boot - (v1.5.22.RELEASE)

- DB setup: MongoDB docker container - latest version from the docker image repository

- compiling and dependency management: Gradle 6.0.1


- Running the following commands in a unix shell will start the application:

	docker run -P -d --name mongodb mongo

		(download the mongo image and create a new docker container from it)
	
	docker ps

		(this will output all docker containers. In my case, for the mongo container under PORTS: 0.0.0.0:32768->27017/tcp.
			The port 32678 on the host is forwared to port 27017 of the container. Copy the first port and use it in the next command to start the application).



	java -Dspring.data.mongodb.uri=mongodb://localhost:32768/micros -jar build/libs/Account.jar




Possible future development:

 - use the cache service decorator - added comments and TODOs in source code
 - facilitate the usage of HTTP caching
 - create a docker container from this microservice using a DockerFile

